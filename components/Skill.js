import Skillset from '../data/mySkills';
import { Container, Row, Col, Card, CardDeck } from 'react-bootstrap';

export default function Skill() {
	const skillData = Skillset.map((skill) => {
		return(
			<Col key={skill.id} md={4}>
				<CardDeck>
					<Card className="m-3 skill__card">
						<Card.Img className="skill__image" bsPrefix="string" variant="top" src={skill.src} />
						<Card.Body className="skill__body">
							<Card.Title className="success" >{skill.name}</Card.Title>
							<Card.Text>Skills Rate: {skill.rating}</Card.Text>
						</Card.Body>
					</Card>
				</CardDeck>
			</Col>
		)
	})
	return (
		<Container className="skill__container">
		<Row id="skills"></Row>
			<Row className="skill__section">
				<h1>Skills</h1>
			</Row>
			<Row >
				{skillData}
			</Row>
		</Container>
	)
}