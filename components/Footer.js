import { Container, Row, Col } from 'react-bootstrap';

export default function Footer() {
	let date = new Date();
	let year = date.getFullYear();
	return (
	  <Container fluid className="footer__container">
	    <Row className="footer__section">
	      <Col md="12" className="footer__text">
	        <p>Designed and Developed by Regie Bartolome</p>
	      </Col>
	      <Col md="12" className="footer__copyright">
	        <p>Copyright © {year}</p>
	      </Col>
	    </Row>
	  </Container>
	);
}