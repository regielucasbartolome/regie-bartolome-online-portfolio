import { Navbar, Nav } from 'react-bootstrap';
//import the nextjs Link component for client-side navigation
import Link from 'next/link';

export default function AppNavBar() {

	return (
		<Navbar expand="lg" className="navigation fixed-top">
		  <Navbar.Brand href="/" className="navbar__brand"><strong>&lt;regie/&gt;</strong></Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
		      <Link href="#up" passHref>
		      	<Nav.Link className="navbar__menu"> Home </Nav.Link>
		      </Link>
		      <Link href="#skills" passHref>
		      	<Nav.Link className="navbar__menu"> Skills </Nav.Link>
		      </Link>
		      <Link href="#projects" passHref>
		      	<Nav.Link className="navbar__menu"> Projects </Nav.Link>
		      </Link>
		      <Link href="#contacts" passHref>
		      	<Nav.Link className="navbar__menu"> Contact </Nav.Link>
		      </Link>
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}

