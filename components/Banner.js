import { Jumbotron, Button, Row, Col } from 'react-bootstrap';
export default function Banner() {
	return (
		<Jumbotron id="up" className="banner__jumbotron">
			<Row>
				<Col>
					<img className="banner__avatar" src="/avatar.jpg" alt="avatar" />
				</Col>
			</Row>
			<Row className="banner__content">
				<Col>
					<h1 className="banner__h1">Hi there!</h1>
					<h2 className="banner__h2">I'm <strong> REGIE BARTOLOME </strong></h2>
					<p className="banner__position">
					  Full Stack Web Developer
					</p>
					<p className="banner__tagline">
					  "Automate your services with <strong>&lt;regie/&gt;</strong>".
					</p>
					<p>
					  <Button className="banner__button">Visit portfolio</Button>
					</p>						
				</Col>
			</Row>
		</Jumbotron>
	)
}