import { Container, Row, Col, Carousel, CardDeck } from 'react-bootstrap';
import Projects from '../data/myProjects';
import Links from 'next/link'

export default function Project() {
	const projectData = Projects.map((project) => {
		return(
			<Carousel.Item key={project.id} className="project__carouselitem">
			    <img
			      className="d-block w-100 h-10"
			      src={project.source}
			      alt={project.name}
			    />
			    <Carousel.Caption className="project__caption d-none d-md-block">
			      <h3 className="project__name">{project.name}</h3>
			      <p className="project__description">{project.description}</p>
			      <Links href={project.url}>		
			      	<a  className="project__link" target="_blank">View project</a>
			      </Links>
			    </Carousel.Caption>
			</Carousel.Item>
		)
	})

	return (
		<Container className="project__container">
			<Row id="projects"></Row>
			<Row className="project__section">
				<h1>Projects</h1>
			</Row>
			<Row>
				<Col>
					<Carousel>
						{projectData}
					</Carousel>
				</Col>
			</Row>
		</Container>		
	)
}