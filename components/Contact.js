import { Container, Form, Button, Row, Col } from 'react-bootstrap';

export default function Contact() {
	return (
		<Container className="contact__container">
			<Row id="contacts"></Row>
			<Row className="contact__section">
					<h1>Contact</h1>
			</Row>
			
			<h2 className="contact__title">Send us your inquiries</h2>
			<Form>
			  <Form.Group controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group controlId="formBasicTextArea">
			      <Form.Label>Message</Form.Label>
			      <Form.Control as="textarea" rows={3} placeholder="Enter your message" />
			   </Form.Group>

			  <Form.Group controlId="formBasicCheckbox">
			    <Form.Check type="checkbox" label="Send copy to my email." />
			  </Form.Group>
			  <Button className="contact__button" type="submit">
			    Submit
			  </Button>
			</Form>
		</Container>
	)
}