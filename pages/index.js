import Head from 'next/head'
import styles from '../styles/Home.module.css'

//components
import Banner from '../components/Banner';
import Skill from '../components/Skill';
import Project from '../components/Project';
import Contact from '../components/Contact';
import Footer from '../components/Footer';

export default function Home() {
  return (
    <>
      <Banner />
      <Skill />
      <Project />
      <Contact />
    </>
  )
}
