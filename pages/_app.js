import React from 'react';
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';
import Animation from '../components/Animation';
import { motion } from "framer-motion";

//components
import AppNavBar from '../components/AppNavbar';
import Footer from '../components/Footer';

function MyApp({ Component, pageProps }) {
  return (
  	<motion.div initial={{ scale: 0 }}animate={{ scale: 1 }} transition={{ ease: "easeOut", duration: 2 }}>
  		<AppNavBar />
  		<Component {...pageProps} />
  		<Footer />
  	</motion.div>
  )
}

export default MyApp
