export default [
	{
		id: 1,
		name: 'National Nutrition Council Website',
		url: `https://www.nnc.gov.ph`,
		source: '/nnc-website.jpg',
		description: 'This is the official website of the National Nutrition Council. The project was completed in 2011 using Content Mangement System.'
	},
	{
		id: 2,
		name: 'East Avenue Medical Center Website',
		url: `http://www.eamc.doh.gov.ph`,
		source: '/eamc-website.jpg',
		description: 'This is the official website of East Avenue Medical Center. The project was completed in 2014 using a Content Management System.'
	},
	{
		id: 3,
		name: `East Avenue Medical Center Citizen's Charter 2020`,
		url: `https://regielucasbartolome.gitlab.io/eamc-citizen-charter`,
		source: '/eamc-citizens-charter.jpg',
		description: 'This is a web application that displays the charter of each department in the hospital. This app was my first capstone project during my bootcamp. This web application was made using HTML, CSS, and Bootstrap.'
	},
	{
		id: 4,
		name: 'East Avenue Medical Center Online Course Booking',
		url: `https://regielucasbartolome.gitlab.io/eamc-courses-booking-online/`,
		source: '/eamc-courses-booking.jpg',
		description: 'This is an online course booking web application. This was develop during my bootcamp. This was made using HTML, CSS, Bootstrap, Javascript, Node.js, and MongoDB.'
	}
]