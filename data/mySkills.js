export default [
	{
		id: 1,
		name: "HTML 5",
		src: "/html.png",
		rating: "⭐⭐⭐⭐/5"
	},
	{
		id: 2,
		name: "CSS 3",
		src: "/css.png",
		rating: "⭐⭐⭐⭐/5"
	},
	{
		id: 3,
		name: "Bootstrap",
		src: "/bootstrap.png",
		rating: "⭐⭐⭐⭐/5"
	},
	{
		id: 4,
		name: "Javascript",
		src: "/javascript.png",
		rating: "⭐⭐⭐/5"
	},
	{
		id: 5,
		name: "React.JS",
		src: "/react.png",
		rating: "⭐⭐⭐/5"
	},
	{
		id: 6,
		name: "Node.js",
		src: "/node.png",
		rating: "⭐⭐⭐/5"
	},
	{
		id: 7,
		name: "Next.JS",
		src: "/next.png",
		rating: "⭐⭐⭐/5"
	},
	{
		id: 8,
		name: "MongoDB",
		src: "/mongodb.png",
		rating: "⭐⭐⭐/5"
	},
	{
		id: 9,
		name: "Git",
		src: "/git.png",
		rating: "⭐⭐⭐⭐/5"
	}
]